import os
import click



def convert(src, trg, volume=1.0, speed=1.0):
    """Conversion utility with option to change volume and speed of an audio"""
    import soundfile as sf
    import librosa
    import numpy as np
    
    data, samplerate = sf.read(src)
    
    if volume!= 1.0:
        data = data * volume
        if np.abs(data).max()>1.0:
            print('Warning! The output is clipped!')
            data = data.clip(min=-1.0, max=1.0)
            
    if speed != 1.0:
        if len(data.shape)>1: # For stereo
            data = librosa.effects.time_stretch(data.T, rate=speed).T
        else:
            data = librosa.effects.time_stretch(data, rate=speed)  # speed is ensured to be > 0 by click

    if os.path.dirname(trg):
        os.makedirs(os.path.dirname(trg), exist_ok=True)
    sf.write(trg, data, samplerate)
    print('Success conversion!')


def transcribe(src, trg, model_name='tiny', device='cpu'):
    """Transcribing utility, using OpenAI Whisper"""
    import json
    
    import whisper
    
    model = whisper.load_model(model_name, device=device)
    result = model.transcribe(src)
    print('Recognition result:', result)
    if os.path.dirname(trg):
        os.makedirs(os.path.dirname(trg), exist_ok=True)
    with open(trg, 'w', encoding='utf-8') as writer:
        writer.write(json.dumps(result, ensure_ascii=False))


@click.command()
@click.argument('cmd', type=click.Choice(['transcribe', 'convert']))
@click.argument('src', type=click.Path(exists=True))
@click.argument('trg', type=click.Path())
@click.option('-v', '--volume', type=click.FloatRange(min=0.), help='Audio volume, (0..1) means lowering volume, (1..inf) means rising volume. Can cause clipping.', default=1.)
@click.option('-s', '--speed', type=click.FloatRange(min=0., min_open=True, ), help='Audio speed, (0..1) means lowering speed, (1..inf) means rising speed. Can cause sound defects.', default=1.)
@click.option('-m', '--model', type=str, help='OpenAI Whisper model_name, or path to checkpoint', default='tiny')
@click.option('-d', '--device', type=str, help='Device to run model on (cpu or cuda)', default='cpu')
def run(cmd, src, trg, volume, speed, model, device):
    """
	Yet Another SoX for sound operations
	
	This utility works in two modes: 'transcribe' and 'convert'.

	Convert mode:
	
	Reads audio file from SRC, and saves to TRG. The the target file ends with .wav, then the file is converted to singed int 16 PCM file. Passing -a or -v options allows to change the volume and speed of file. The sampling frequency and pitch rests the same.

	Transcribe mode:
	
	Reads audio file from SRC, transcribes it with OpenAI-Whisper? and saves result in TRG JSON file. You can select whisper model using -m option, list of models can be found on https://github.com/openai/whisper, or download checkpoint and use it. If your environement is proprely set, you can pass computing device for whisper, using -d option


	"""
    if cmd == 'convert':
        convert(src, trg, volume, speed)
    else: 
        transcribe(src, trg, model, device)


if __name__=='__main__':
    run()