# yasox_demo

Simple CLI program for operating with audio files.

For now can:
1. Convert audio files and make simple postprocessing (changing volume und speed)
2. Transcribe audio (i.e. run Speech-to-Text or Audio Speech Recognition) and save result in json.

## Installation

* You need Python3.9 or higher to run this code;
* You need to instal `ffmpeg` to run this code 
* For running ASR-service you need to install `https://github.com/openai/whisper`; 
* If you have a GPU and want to use it, you need to proprely install `PyTorch`, matching your hardware as well; 
* For general audio operations you need to isntall `soundfile` (and corresponding libraries such as `libsndfile` as well); 
* For time-stretching you need to install `librosa`;
* Command-Line-Interface is implemented using excellent `click` library

Most straight-forward, but not always working way is to install everything via requirements:

```
pip install -r requirements.txt
```

## Launching
General help can be found by using
```
python yasox.py --help

```
The two main ways of running the service are the following:

### Converting

```
python yasox.py convert SRC TRG -v {VLM} -s {SPD}
```

* `SRC` -- source audio file, most audio file types are okey
* `TRG` -- target audio file. If target extension is `'.wav`, result encoding will be `signed 16 bit integer le` (this can be unintenional!). If target file extension doesn't match source file extension, conversion will occur.
* `VLM` -- volume modifier, from 0 (silence) to infinty, 1 for no volume change. big values for volume can cause clipping and distortion of a signal.
* `SPD` -- audio speed modifier, also from 0 (eternity) to infinity (blink), 1 as no change. Changing of speed is done not via resampling, and because of that there is: a. no change of pitch and b. big speed modifierts will cause sound artifacts.

### Transcribing

```
python yasox.py transcribe SRC TRG -m {MDL} -d {DVC}
```

* `SRC` -- source audio file, most audio file types are okey
* `TRG` -- target file with regard. disregarding extension, it will be text file in `utf-8` with transcription from whisper model in JSON format. Output JSON object contains `text` field with transcribed text, `language` field with detected language, and `segments` field with fine segmentation for transcription and additional info.
* `MDL` -- model_name for whisper. it can be supported model name (the model will be downloaded and cached by the whisper), or path to checkpoint (you can download model yourself, exemple, from `https://huggingface.co/collections/openai/whisper-release-6501bba2cf999715fd953013`)
* `DVC` -- device to run whisper on. By default it is `cpu`, if you have cuda, you can use pytorch-notation to pass device.

## Test Data
This repository contains some samples from free sources to test repo. Samples are hosted using `git-lfs`. 
The sources for samples are:
1. LJSpeech Dataset `https://keithito.com/LJ-Speech-Dataset/`
2. CommonVoice Dataset, Russian 16.1 Delta Segment `https://commonvoice.mozilla.org/en/datasets`

The files are stored in `audio` directory.
In `audio/test_samples` you can see additional versions of file, created using different encoding wariants of `wave` files, like 64 float, 32 float, signed 16 int, signed 32 int, mulaw, and signed16 in stereo.  
`audio/meta.csv` is a file, containing ground-truth transcriptions.  
`audio/null.wav` is an audio file with no speech in it, added for purposes of testing.

## License
MIT

